/* global require, module, console */
"use strict";
var fs = require('fs');
var path = require('path');
var glob = require('glob');

var parseString = require('xml2js').parseString;

function SVGPathParser (pathString) {

	var _this = this;
	var _currentIndex;
	var _pathString = pathString;
	var _currentPoint;
	var _previousControlPoint;

	var _delims = ["h", "H", "V", "v", ",", "C", "s", "S", "c", "l", "L"];

	function isDelim (character) {
		for (var i = 0; i < _delims.length; i++) {
			if (_delims[i] === character) {
				return true;
			}
		}

		return false;
	}

	function getNextNumberInPath() {
		var start = _currentIndex;
		var end = _currentIndex;
		var index = start;

		// loop through the characters until we're done with this number.
		while (index < _pathString.length && !isDelim(_pathString[index]) && !(_pathString[index] == "-" && index > start)) {
			index++;
			end = index;
		}

		if (index < _pathString.length && _pathString[index] == "-") {
			_currentIndex = end;
		} else {
			_currentIndex = end + 1;
		}

		return parseFloat(_pathString.substring(start, end));
	}

	function _hasNextSegment() {
		return _currentIndex < _pathString.length;
	}

	function convertRelativeXToAbsolute(startPoint, relativePoint) {
    	return relativePoint + startPoint[0];
	}

	function convertRelativeYToAbsolute(startPoint, relativePoint){
    	return relativePoint + startPoint[1];
    }

	function setupCSegment(startPoint) {
		var x1 = getNextNumberInPath();
  		var y1 = getNextNumberInPath();
    	var x2 = getNextNumberInPath();
    	var y2 = getNextNumberInPath();
    	var x = getNextNumberInPath();
    	var y = getNextNumberInPath();
    	return [startPoint, [x1, y1], [x2, y2], [x, y]];
	}

	function setupcSegment(startPoint){
    	var x1 = convertRelativeXToAbsolute(startPoint, getNextNumberInPath());
    	var y1 = convertRelativeYToAbsolute(startPoint, getNextNumberInPath());
    	var x2 = convertRelativeXToAbsolute(startPoint, getNextNumberInPath());
    	var y2 = convertRelativeYToAbsolute(startPoint, getNextNumberInPath());
    	var x = convertRelativeXToAbsolute(startPoint, getNextNumberInPath());
    	var y = convertRelativeYToAbsolute(startPoint, getNextNumberInPath());
    	return [startPoint, [x1, y1], [x2, y2], [x, y]];
    }

    function setupSsegment(startPoint, previousControlPoint) {
    	var x1 = - (previousControlPoint[0] - startPoint[0]) + startPoint[0];
    	var y1 = - (previousControlPoint[1] - startPoint[1]) + startPoint[1];
    	var x2 = getNextNumberInPath();
    	var y2 = getNextNumberInPath();
    	var x = getNextNumberInPath();
    	var y = getNextNumberInPath();

    	return [startPoint, [x1, y1], [x2, y2], [x, y]];
    }


    function setupsSegment(startPoint, previousControlPoint) {
    	var x1 =  - (previousControlPoint[0] - startPoint[0]) + startPoint[0];
    	var y1 =  - (previousControlPoint[1] - startPoint[1]) + startPoint[1];
    	var x2 = convertRelativeXToAbsolute(startPoint, getNextNumberInPath());
    	var y2 = convertRelativeYToAbsolute(startPoint, getNextNumberInPath());
    	var x = convertRelativeXToAbsolute(startPoint, getNextNumberInPath());
    	var y = convertRelativeYToAbsolute(startPoint, getNextNumberInPath());

    	return [startPoint, [x1, y1], [x2, y2], [x, y]];
    }

    function setuplSegment(startPoint) {
    	var x = convertRelativeXToAbsolute(startPoint, getNextNumberInPath());
    	var y = convertRelativeYToAbsolute(startPoint, getNextNumberInPath());
    	return [startPoint, [x, y]];
    }

    function setupLsegment(startPoint) {
    	var x = getNextNumberInPath();
    	var y = getNextNumberInPath();
    	return [startPoint, [x, y]];
    }


    function setuphSegment(startPoint) {
    	var x = convertRelativeXToAbsolute(startPoint, getNextNumberInPath());
    	var y = startPoint[1];
    	return [startPoint, [x, y]];
    }

    function setupHSegment(startPoint) {
    	var x = getNextNumberInPath();
    	var y = startPoint[1];
    	return [startPoint, [x,y]];
    }

    function setupvSegment(startPoint) {
    	var x = startPoint[0];
    	var y = convertRelativeYToAbsolute(startPoint, getNextNumberInPath());
    	return [startPoint, [x,y]];
    }

    function setupVsegment(startPoint){
   	 	var x = startPoint[0];
   	 	var y = getNextNumberInPath();
  	 	return [startPoint, [x,y]];
  	 }


	function _getNextSegment() {
		var startPoint = _currentPoint.slice(0);
		var previousControlPoint = _previousControlPoint.slice(0);
		var segment = [];

		var currentChar = _pathString[_currentIndex -1];
		switch(currentChar) {
			case "C":
				segment = setupCSegment(startPoint);
			break;
			case "c":
				segment = setupcSegment(startPoint);
			break;
			case "S":
				segment = setupSsegment(startPoint, previousControlPoint);
			break;
			case "s":
				segment = setupsSegment(startPoint, previousControlPoint);
			break;
			case "l":
				segment = setuplSegment(startPoint);
			break;
			case "L":
				segment = setupLsegment(startPoint);
			break;
			case "h":
				segment = setuphSegment(startPoint);
			break;
			case "H":
				segment = setupHSegment(startPoint);
			break;
			case "v":
				segment = setupvSegment(startPoint);
			break;
			case "V":
				segment = setupVsegment(startPoint);
			break;
		}

		if(segment.length > 2) {
			_previousControlPoint = segment[2];
			_currentPoint = segment[3];
		} else {
			_previousControlPoint = segment[1];
			_currentPoint = segment[1];
		}
		return segment;
	}

	_this.parseSVGPath = function() {
		_currentIndex = 1;
		var segments = [];
		_currentPoint = [getNextNumberInPath(_pathString), getNextNumberInPath(_pathString)];
		_previousControlPoint = _currentPoint.slice(0);


		while (_hasNextSegment()) {
			segments.push(_getNextSegment());
		}

		return segments;
	};

}

module.exports = function (inputFolder, outputFolder, includeIdAndClass) {

	var returnString;

	glob(inputFolder + "**/*.svg", {}, function (er, files) {

		for (var fI = 0; fI < files.length; fI++) {
			var fileName = files[fI];
			var pathToSVG = fileName;
			var xml = fs.readFileSync(pathToSVG);

			parseString(xml, function (err, result) { // jshint ignore:line

				var paths = result.svg.path;
				var lines = result.svg.line;
				var polylines = result.svg.polyline;

				var numPaths = 0;
				var numLines = 0;
				var numPolyLines = 0;
				if(paths !== undefined) {
					numPaths = paths.length;
				}
				if(lines !== undefined) {
					numLines = lines.length;
				}
				if(polylines !== undefined) {
					numPolyLines = lines.length;
				}

				if(numPaths + numLines + numPolyLines > 1) {
					returnString = "[";
				} else {
					returnString = "";
				}
				if(paths !== undefined) {
					for (var i =0; i < paths.length; i++) {
						var id = paths[i].$.id;
						var cls = paths[i].$['class'];

						var svgString = paths[i].$.d.trim().replace ( /[\s]/gm, "" );
						var parser = new SVGPathParser((svgString));

						var resultString;

						if(includeIdAndClass) {
							var obj = {
								'id': id,
								'class': cls,
								'path': parser.parseSVGPath()
							}
							resultString = JSON.stringify(obj);
						} else {
							resultString = JSON.stringify(parser.parseSVGPath());
						}



						if(i < paths.length - 1) {
							returnString += "\t"+resultString + ",";
						} else {
							returnString += "\t"+resultString;
						}
					}
				}

			 	

				if(numLines > 0) {
					returnString += ",";
				}

				if(lines !== undefined) {
					for (var i =0; i < lines.length; i++) {
						var id = lines[i].$.id;
						var cls = lines[i].$['class'];

						var x1 = parseFloat(lines[i]["$"]['x1']);
						var x2 = parseFloat(lines[i]["$"]['x2']);
						var y1 = parseFloat(lines[i]["$"]['y1']);					
						var y2 = parseFloat(lines[i]["$"]['y2']);

						var lineArray = [[x1, y1],[x2,y2]];

						var resultString;
						if(includeIdAndClass) {
							var obj = {
								'id': id,
								'class': cls,
								'path': lineArray
							}
							resultString = JSON.stringify(obj);
						} else {
							resultString = JSON.stringify(lineArray);
						}

						if(i < lines.length - 1) {
							returnString += "\t"+resultString + ",";
						} else {
							returnString += "\t"+resultString;
						}
					}

				}

				if(numPolyLines > 0) {
					returnString += ",";
				}

				if(polylines !== undefined) {
					for (var i =0; i < polylines.length; i++) {
						var id = polylines[i].$.id;
						var cls = polylines[i].$['class'];
						
						var polyLine = [];

						var points = polylines[i]["$"].points.split(" ");

						for(var j = 0; j < points.length - 2; j++) {
							if(points[j].length > 0) {
								var xy = points[j].split(",");
								var x1 = parseFloat(xy[0]);
								var y1 = parseFloat(xy[1]);

								var xy2 =  points[j+1].split(",");
								var x2 = parseFloat(xy2[0]);
								var y2 = parseFloat(xy2[1]);

								var pair = [[x1, y1], [x2, y2]];
								polyLine.push(pair);

							}
						}

						var resultString;
						if(includeIdAndClass) {
							var obj = {
								'id': id,
								'class': cls,
								'path': polyLine
							}
							resultString = JSON.stringify(obj);
						} else {
							resultString = JSON.stringify(polyLine);
						}

						if(i < polylines.length - 1) {
							returnString += "\t"+resultString + ",";
						} else {
							returnString += "\t"+resultString;
						}
					}
				}

				if(numPaths + numLines + numPolyLines > 1) {
						returnString += "]";
				}


				var outputFile = path.join(outputFolder, path.basename(fileName).slice(0, -4) + ".json");
				fs.writeFileSync(outputFile, returnString);



			});

		}
	});

};

// uncomment to test on the command line by running
// node index.js
//module.exports("exampleSVGs/", "exampleOutput/", true);
